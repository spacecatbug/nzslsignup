## Introduction

A program that helps people to learn new languages through a variety of games: a multichoice quiz, a card matching game and a game involving computer vision detection.

Vision detection is used for learning New Zealand Sign Language. The player has to sign the correct gestures using a webcam to score points. This application uses visual intelligence to recognise the New Zealand Sign Language gestures.

This application is written in Java and follows the MVC design pattern. OpenCV and DeepLearning4j are used to for the visual detection for the Sign gestures, Swing is used to build the GUI and the Hibernate framework is used for database management.

### Languages included:

- French
- Spanish
- German
- New Zealand Sign Language

![link to demo video](./src/main/resources/LanguageLearnDemo.gif)