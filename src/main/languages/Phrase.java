package languages;


/**
 * This interface is used to standardise the methods for the Enum classes in the 
 * languages package. It is also used to create a relationship of inheritance so
 * that this interface acts as an abstract superclass for Enum class types which
 * are, by definition, final, so inheritance cannot be implemented using just an
 * abstract class. 
 * @author MYWT
 */
public interface Phrase {
    
    /**
     * Returns the pronounication of the (foreign) phrase.
     * @return Returns the pronounication of the (foreign) phrase.
     */
    public String getPronounciation();
    
    /**
     * Returns the Enum value which represents the English phrase, formatted to a 
     * readable String format.
     * 
     * @return Returns the Enum value which represents the English phrase, formatted 
     * to a readable String format.
     */
    @Override
    public String toString();
    
    /**
     * Returns the name of the language and is used for users to select languages of
     * Phrases to be tested on when running a Standard game.
     * @return Returns the name of the language in String format.
     */
    public String getLanguage();
    
    
    /**
     * Returns the phrase in the foreign language.
     * @return Returns the phrase in the foreign language.
     */
    public String getForeignPhrase();
}
