package main;

import java.util.*;

/**
 * This class manages Game objects which are used to run a game session.
 * A Game contains a QuestionSet which define the questions and answers of a
 * Game. The languages to be tested on in a Game can be set by user input.
 * A Game's QuestionSet can also be defined by a text file input.
 * 
 * When a Game ends the score is saved into the ScoreBoard.
 * 
 * @author MYWT
 */
public class Game {
    private boolean inProgress;
    private int score;
    private QuestionSet questionSet;
    private int questionCounter = 1;
    private Scanner keyboard;
    private String filePath = "";
    
    /**
     * This (overloaded) constructor is used to set up a custom game derived from text file info.
     * @param keyboard Scanner object used for the main CUI
     * @param filePath String path to text file containing Phrases and languages
     */
    public Game(Scanner keyboard, String filePath) {
        this.inProgress = true;
        this.score = 0;
        this.questionSet = new QuestionSet();
        this.keyboard = keyboard;
        this.filePath = filePath;
    }
    
    /**
     * This (overloaded) constructor is used to set up a standard game (and does
     * not require a text file path)
     * @param keyboard Scanner object used for the main CUI
     */
    public Game(Scanner keyboard) {
        this.inProgress = true;
        this.score = 0;
        this.questionSet = new QuestionSet();
        this.keyboard = keyboard;
    }
    
    /**
     * Runs a Game by displaying questions and multi-choice answers, receiving answers
     * from user input and saving scores onto the ScoreBoard when the Game has ended.
     */
    public void runGame() {
        /* If a file path is specified, use the file to define the list of possible
        questions to use to create the QuestionSet */
        if (!this.filePath.equals("")) {
            this.questionSet.setPhrasesFromFile(filePath);
        } else {
            setLanguagesToTest();
        }
        this.questionSet.generateQuestionSet();

        int nQuestions = this.questionSet.getMultiChoiceQuestions().size();
        
        // Iterate through all questions in the Question Set for the game
        for (MultiChoiceQuestion mcq: questionSet.getMultiChoiceQuestions()) {
            System.out.println("Question " + questionCounter + " out of " + nQuestions +":");
            // Display multichoice answers
            System.out.println(mcq.MultiChoiceToString());
            String response = keyboard.nextLine();
            if (mcq.getCorrectAnswerChar().equalsIgnoreCase(response)) {
                score++;
                System.out.println("Correct!");
            } else {
                System.out.println("Incorrect");
            }
            // Display info for the test phrase, translation and pronounication
            System.out.println(mcq.printCorrectAnswer()+"\n");
            questionCounter++;
        }
        
        System.out.println("Game ended. Your score is: " + score + " out of " + nQuestions);
        Scoreboard.saveScore(score);
        System.out.println("This score has been saved to the scoreboard");
        System.out.println("Press any key to return to the main manu");
        keyboard.nextLine();
    }    
    
    /**
     * Used to select the languages that will be included for a standard game i.e.
     * Phrases are not defined from a text file input.
     */
    public void setLanguagesToTest() {
        boolean languageIsSet = false;
        String languagesChosenMessage = "\nLanguages selected: ";
        System.out.println("Please select the languages to include in this game separated by commas");
        System.out.println("e.g. if you would like to include Spanish and French, please enter 1,2");
        System.out.println("1) Spanish, 2) French, 3) German");
        String response = keyboard.nextLine().trim();
        
        String[] fileLineArr = response.split(",[ ]*");
        ArrayList<String> languageSelection = new ArrayList<>();
        for (String s: fileLineArr) {
            languageSelection.add(s.replace(" ", "").trim());
        }
        
        if (languageSelection.contains("1")) {
            this.questionSet.addLanguagePhrases("spanish");
            languageIsSet = true;
            languagesChosenMessage+="\nSpanish";
        }
       
        if (languageSelection.contains("2")) {
            this.questionSet.addLanguagePhrases("french");
            languageIsSet = true;
            languagesChosenMessage+="\nFrench";
        }
        
        if (languageSelection.contains("3")) {
            this.questionSet.addLanguagePhrases("german");
            languageIsSet = true;
            languagesChosenMessage+="\nGerman";
        }
        
        if (!languageIsSet) {
            languagesChosenMessage = "No valid language selections made - question set is set to default to test all languages";
            this.questionSet.addAllPhrases();
        }
        System.out.println(languagesChosenMessage+"\n");
    }
    
    /**
     * @return Returns the score which represents the number of questions that have 
     * been answered correctly in a Game session.
     */
    public int getScore() {
        return score;
    }
    
    /**
     * @param score Sets the score which represents the number of questions that 
     * have been answered correctly in a Game session.
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * @return Returns true if a Game session is still in progress i.e. all 
     * Multi Choice Questions have not been answered in the QuestionSet via user input.
     * Returns false if a Game session has ended.
     */
    public boolean getInProgress() {
        return inProgress;
    }
    
    /**
     * 
     * @param inProgress Sets the InProgress boolean flag. A True value represents
     * that a Game session is still in progress i.e. all Multi Choice Questions
     * have not been answered in the QuestionSet via user input. False represents 
     * that a Game session has ended.
     */
    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }
}
