package main;

import java.util.Scanner;

/**
 * This is the main method to run this application as a command line user
 * interface. This application is a language learning game which is a multiple
 * choice quiz game which tests the English translations of foreign phrases.
 * 
 * Features of this application:
 * - Users can choose which languages to be tested on when running a quiz.
 * - A custom quiz can be built by providing phrases from a text file.
 * - The game will randomly generate the questions and multiple choice answers
 *   so that the questions/answers will not always be consistent/repetitive.
 * - User can view past score history from previous games played.
 *
 * @author MYWT
 */
public class RunMainCUI {

    public static Scanner keyboard;
    private static Scoreboard scoreboard;
    private static boolean run;
    private static Game game;
    
    /**
     * Main CUI object instantiates a Scoreboard object to track scores and a 
     * Scanner object to process user input.
     */
    public RunMainCUI() {
        System.out.println("Welcome to this language learning game! (CUI version)\n");
        keyboard = new Scanner(System.in);
        scoreboard = new Scoreboard();
        run = true;
    }

    /**
     * The main menu for the CUI
     */
    public static void mainMenu() {
        System.out.println("\nMain menu\n\n"
                + "1) New Game\n"
                + "2) About this game\n"
                + "3) View scoreboard\n"
                + "4) Quit\n\n"
                + "Please type in a menu option number and press Enter:");

        String userInput = keyboard.nextLine();
        

        if (userInput.equals("1")) {
            gameSetup();
        } else if (userInput.equals("2")) {
            gameDescription();
        } else if (userInput.equals("3")) {
            viewScoreBoard();
        } else if (userInput.equals("4")) {
            run = false;
        } else {
            System.out.println("\nInvalid input, please try again");
        }
    }

    /**
     * The game menu for the CUI. Users can select whether to play a standard game
     * or a custom game. In a standard game the user selects which languages to
     * test on, then all available Phrases for the selected languages are included
     * in the quiz. In a custom game the phrases (and the languages they belong to)
     * are defined in a text file.
     */
    public static void gameSetup() {
        boolean gameSetupRun = true;
        String gameSetupMessage = "Select game type:\n"
                + "1) Standard game\n"
                + "2) Custom game (load from text file)\n"
                + "3) Go back to main menu\n"
                + "Please type in a menu option number and press Enter:";
        System.out.println(gameSetupMessage);

        String gameSelection = keyboard.nextLine();
        while (gameSetupRun) {
            if (gameSelection.equals("1")) {
                game = new Game(keyboard);
                System.out.println("Starting a new standard game...\n");
                game.runGame();
                gameSetupRun = false;
            } else if (gameSelection.equals("2")) {
                System.out.println("Load game from file");
                game = new Game(keyboard,"customgame.txt");
                game.runGame();
                gameSetupRun = false;
            } else if (gameSelection.equals("3")) {
                gameSetupRun = false;
            } else {
                System.out.println("Invalid input, try again\n");
                System.out.println(gameSetupMessage);
                gameSelection = keyboard.nextLine();
            }
        }
        mainMenu();
        }
    
    /**
     * Displays game information and instructions for the user.
     */
    public static void gameDescription() {
        System.out.println
                ("This is a game that helps you to learn basic phrases in different\n"
                + "languages. The game is a quiz with 20 questions and the user can select which.\n"
                + "languages to test. Type in the multichoice letters and press enter to answer the\n"
                + "questions. You can create a custom quiz to focus your learning on selected phrases\n"
                + "by providing a text file with the English phrases to choose the test questions.\n\n"
                + "Press any key to return to the main menu.");
        keyboard.nextLine();
    }
    
    /**
     * Displays the score history, showing the score received and the time/date 
     * games were completed.
     */
    public static void viewScoreBoard() {
        System.out.println(scoreboard);
        System.out.println("Press any key to return to the main menu.");
        keyboard.nextLine();
    }

    /**
     * This is the main method that executes the application
     */
    public static void main(String[] args) {
        RunMainCUI runMain = new RunMainCUI();
        while (run) {
            mainMenu();
        }

        System.out.println("Program exited.");
        System.exit(0);
    }
}
