package main;

import languages.Phrase;

/**
 * This class manages Question objects which are used to make up the test question
 * and 'incorrect' multiple-choice answers in a MultiChoiceQuestion object.
 * 
 * @author MYWT
 */
public class Question {
    private Phrase phrase;
    private boolean isTestQuestion;
    
    /**
     * Question objects are instantiated with:
     * 1) Phrase value which contains the foreign phrase, English translation and 
     *    pronounication information.
     * 2) A boolean flag indicating whether the question is the 'correct' test
     *    question for a MultiChoiceQuestion object or not (i.e. an 'incorrect'
     *    multi-choice answer).
     *    
     * @param phrase A Phrase value containing the foreign phrase, English 
     * translation and pronounication information.
     * @param isTestQuestion A boolean flag indicating whether the question is 
     * the 'correct' test question for a MultiChoiceQuestion object or not (i.e.
     * an 'incorrect' multi-choice answer).
     */
    public Question(Phrase phrase, boolean isTestQuestion) {
        this.phrase = phrase;
        this.isTestQuestion = isTestQuestion;
    }
    
    /**
     * Returns a String representation of the question and is used for display in
     * the CUI when a Game is running.
     * @return Returns a String representation of the question and is used for
     * display in the CUI when a Game is running.
     */
    @Override
    public String toString() {
        String questionMessage = String.format("How do you say this in %s? \n\"%s\"", this.phrase.getClass().getSimpleName(),this.getPhrase());
        return questionMessage;
    }
    
    /**
     * Returns the Phrase enum value belonging to a Question object.
     * @return Returns the Phrase enum value belonging to a Question object.
     */
    public Phrase getPhrase() {
        return phrase;
    }

    /**
     * Sets the Phrase enum value for a Question object.
     * @param phrase Sets the Phrase enum value for a Question object.
     */
    public void setPhrase(Phrase phrase) {
        this.phrase = phrase;
    }

    /**
     * Returns a boolean value indicating whether the Question object is the 
     * 'correct' test question or not. If this value is false, this means that this
     * object is meant to be an 'incorrect' multiple choice answer.
     * @return Returns a boolean value indicating whether the Question object is the 
     * 'correct' test question or not.
     */
    public boolean getIsTestQuestion() {
        return isTestQuestion;
    }

    /**
     * If this value is set to True, it indicates that the Question object is the 
     * 'correct' test question. If this value is set to false, this means that
     * this object is meant to be an 'incorrect' multiple choice answer.
     * @param isTestQuestion A boolean value indicating whether the Question object is the 
     * 'correct' test question or not.
     */
    public void setIsTestQuestion(boolean isTestQuestion) {
        this.isTestQuestion = isTestQuestion;
    }

}
