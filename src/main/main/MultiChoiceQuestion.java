package main;

import languages.Phrase;
import languages.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class maintains MultiChoiceQuestion objects which consist of one (correct) 
 * test Question and multiple (incorrect) answer. MultiChoiceQuestion objects are 
 * made up of Question objects. QuestionSet objects are made up of 
 * MultiChoiceQuestion objects.
 * 
 * @author MYWT
 */
public class MultiChoiceQuestion {
    private List<Question> questions;
    private Question testQuestion;
    private String correctAnswerChar;
    private static List<Phrase> allPhrases = new ArrayList<>();
    private final int nMultiChoiceAnswers = 4;

    /**
     * When a MultiChoiceQuestion is initialized all phrases from all languages
     * are added to allPhrases, which is a list of phrases that is used to select
     * the 'incorrect' answers for the MultiChoiceQuestion.
     */
    public MultiChoiceQuestion() {
        this.questions = new ArrayList<>();
        allPhrases.addAll(Spanish.getAllPhrases());
        allPhrases.addAll(French.getAllPhrases());
        allPhrases.addAll(German.getAllPhrases());
    }
    
    /**
     * Returns the text representation of the MultiChoiceQuestion for displaying
     * in the CUI when a Game is run
     * @return The String format representation of the question and multi-choice
     * answers.
     */
    public String MultiChoiceToString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.printQuestion());
        sb.append("\n");
        sb.append(this.printAnswers());
        return sb.toString();
    }
    
    /**
     * Returns the text representation of the testQuestion for a MultiChoiceQuestion.
     * Used to display the test question in the CUI when a Game is run.
     * @return The String format representation of the test question of a 
     * MultiChoiceQuestion object.
     */
    public String printQuestion() {
        return this.testQuestion.toString();
    }
    
    /**
     * Returns the text representation of the multiple choice answers in a 
     * MultiChoiceQuestion. Used to display the answers in the CUI when a Game is
     * run. Each multiple-choice answer is labelled alphabetically and the correct
     * answer is stored into the correctAnswerChar attribute for this MultiChoiceQuestion 
     * object.
     * @return The String format representation of the multiple choice answers for a 
     * MultiChoiceQuestion object.
     */
    public String printAnswers() {
        String answers = "";
        int choice = 65;
        for (Question q: this.questions) {
            String choiceChar = String.valueOf((char) choice);
            answers += choiceChar +") " + q.getPhrase().getForeignPhrase() +"\n";
            if (q.getIsTestQuestion()) {
                this.correctAnswerChar = choiceChar;
            }
            choice++;
        }
        return answers;
    }
    
    /**
     * Returns the text representation of the answer to the testQuestion for a 
     * MultiChoiceQuestion. It is used to display the correct in the CUI in a Game
     * to educate the user about the foreign phrase after the user has answered the
     * MultiChoiceQuestion. It includes the foreign phrase, the pronounication of the
     * Phrase and also its correct English translation.
     * @return The String format representation of the correct answer to a 
     * MultiChoiceQuestion object.
     */
    public String printCorrectAnswer() {
        Phrase testPhrase = this.testQuestion.getPhrase();
        String correctAnswerStr = String.format("\"%s\" in %s is \"%s\"\n(pronounication: \"%s\")",
                testPhrase.toString(), testPhrase.getLanguage(), testPhrase.getForeignPhrase(), testPhrase.getPronounciation());
        return correctAnswerStr;
    }
    
    /**
     * Adds a new Question to the list of Questions that make up the 
     * MultiChoiceQuestion object. When a Question is added to this list, it is
     * checked to see if it is a test question (defined by the boolean attribute 
     * isTestQuestion). If it is the test question, this Question is also set 
     * as the testQuestion attribute for this MultiChoiceQuestion object.
     * @param question Question to be added to the list of Questions in the 
     * MultiChoiceQuestion object.
     */
    public void addQuestion(Question question) {
        this.questions.add(question);
        if (question.getIsTestQuestion()) {
            this.testQuestion = question;
        }
    }
    
    /**
     * Randomly selects the incorrect multiple choice answers, selected from all
     * available Phrases in all languages. All answers are then randomly shuffled
     * to randomise the order they appear which also randomises the correct 
     * answer letter (i.e. A,B,C or D).
     */
    public void setIncorrectAnswers() {
        Collections.shuffle(allPhrases);
        while (this.questions.size() < nMultiChoiceAnswers) {
            for (Phrase phrase: allPhrases) {
                if (!this.testQuestion.getPhrase().equals(phrase)) {
                    this.addQuestion(new Question(phrase,false));
                }
            }
        }
    }
    
    /**
     * Returns a list of Question objects which consist of one test Question (and
     * correct answer) and multiple 'incorrect' multiple-choice answers.
     * @return Returns a list of Question objects stored in the MultiChoiceQuestion
     * object.
     */
    public List<Question> getQuestions() {
        return questions;
    }
    
    /**
     * Sets an ArrayList of Question objects for a MultiChoiceQuestion object.
     * If the Question object is set as the test question, defined by a boolean
     * attribute, it is also set as the testQuestion for the MultiChoiceQuestion
     * object.
     * @param questions An ArrayList of Question objects.
     */
    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
        for (Question q: questions) {
            if (q.getIsTestQuestion()) {
                this.testQuestion = q;
            }
        }
    }

    /**
     * Returns the Question object to be tested for this MultiChoiceQuestion object.
     * @return Returns the Question object to be tested for this MultiChoiceQuestion object.
     */
    public Question getTestQuestion() {
        return this.testQuestion;
    }
    
    /**
     * Takes a Question object as a parameter, sets the boolean variable belonging
     * to the Question indicating that it is a test question, then defines the 
     * testQuestion attribute for this MultiChoiceQuestion object to be this Question.
     * @param testQuestion A Question object to define as the test Question for this
     * MultiChoiceQuestion object
     */
    public void setTestQuestion(Question testQuestion) {
        testQuestion.setIsTestQuestion(true);
        this.testQuestion = testQuestion;
    }
    
    /**
     * Returns the letter representing the correct answer to the testQuestion 
     * for a MultiChoiceQuestion i.e. A, B, C or D.
     * @return Returns the letter representing the correct answer i.e. A, B, C or D.
     */
    public String getCorrectAnswerChar() {
        return correctAnswerChar;
    }

    /**
     * Sets the letter representing the correct answer to the testQuestion for a
     * MultiChoiceQuestion (i.e. A, B, C or D). This method is used after the list 
     * consisting of the testQuestion and multiple choice answers have been set.
     */
    public void setCorrectAnswerChar() {
        int choice = 65; // Ascii value for "A"
        for (Question q: this.questions) {
            if (q.getIsTestQuestion()) {
                this.correctAnswerChar = String.valueOf((char)choice);
                // Iterate through the alphabetical letters via ascii integer values
                choice++; 
                break;
            }
        }
    }
}