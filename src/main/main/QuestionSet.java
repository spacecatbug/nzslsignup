package main;

import languages.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

/**
 * This class manages QuestionSet objects which represent the information for the
 * multi-choice questions and answers that are used when running a Game.
 * 
 * The list of Phrases to use for a QuestionSet are set in two ways, either:
 * 1) Selection of languages to include in the game via user input OR
 * 2) Custom list of Phrases as defined by a text file
 * 
 * The MultiChoiceQuestions are selected at random from a list of Phrases to create
 * the QuestionSet used for a Game.
 * 
 * @author MYWT
 */
public class QuestionSet {
    private List<MultiChoiceQuestion> multiChoiceQuestions;
    private List<Phrase> phraseBank;
    private final List<Phrase> allPhrases; 
    private final int nQuestions = 20; // Total number of questions in a quiz
    private final int nMultiChoiceQs = 4; // Total number of multi-choice answers provided per question
    private Scanner scanReader;
    

    public QuestionSet() {
        this.multiChoiceQuestions = new ArrayList<MultiChoiceQuestion>();
        allPhrases = new ArrayList<>();
        phraseBank = new ArrayList<>();
    }
    
    /**
     * Adds all Phrase values for all languages representing the list of Phrases
     * that will be selected at random to define the MultiChoice test questions
     * that are generated for a Game. When the input for the list of Phrases is
     * invalid i.e. no valid Phrases are in the Phrase bank, then all Phrases 
     * for all languages are added to the phrase bank.
     */
    public void addAllPhrases() {
        phraseBank.addAll(Arrays.asList(Spanish.values()));
        phraseBank.addAll(Arrays.asList(French.values()));
        allPhrases.addAll(Arrays.asList(German.values()));
    }

    /**
     * Used to set a custom bank of phrases from a text file input representing the
     * list of Phrases that can be used for random selection to define the 
     * MultiChoiceQuestions used in a Game. The text file must contain one phrase 
     * per line, with the Enum value first then the language, separated by comma.
     * @param filePath The file path to the text file input in a String format.
     */
    public void setPhrasesFromFile(String filePath) {
        
        HashSet<Phrase> phraseSet = new HashSet<>();
        
        try {
            scanReader = new Scanner(new FileReader(filePath));
            while (scanReader.hasNextLine()) {
                String fileLineStr = scanReader.nextLine().toUpperCase();
                String[] fileLineArr = fileLineStr.split(",[ ]*");
                String className = "";
                try {
                    className = fileLineArr[1];
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue; // Line is not comma delimited so it is invalid - skip to next line
                }
                
                if (isClass(className)) {
                    String foreignPhrase = fileLineArr[0];
                    if (className.equalsIgnoreCase("SPANISH")) {   
                        try {
                            if (allPhrases.contains(Spanish.valueOf(foreignPhrase))) {
                                phraseSet.add(Spanish.valueOf(foreignPhrase));
                            }
                        } catch (IllegalArgumentException e) {
                            // Invalid phrase, do nothing, do not add to phrase bank
                        }
                    }
                    else if (className.equalsIgnoreCase("FRENCH")) {   
                        try {
                            if (allPhrases.contains(French.valueOf(foreignPhrase))) {
                                phraseSet.add(French.valueOf(foreignPhrase));
                            }
                        } catch (IllegalArgumentException e) {
                            // Invalid phrase, do nothing, do not add to phrase bank
                        }
                    }
                }
                // The phraseBank contains a list of unique Phrases
                this.phraseBank.addAll(phraseSet);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error: check local file path: " + e);
        } finally {
            scanReader.close();
        }
    }
    
    /**
     * Used to determine whether or not an input String value is a valid language of
     * Phrases available for this program (i.e. is the input String a valid enum class
     * name inside the languages package). Returns true if the language does exist as 
     * an Enum class and false if it does not.
     * @param className A String input representing a language class name
     * @return Returns true if the language does exist as an Enum class and false if it does not.
     */
    private boolean isClass(String className) {
        try {
            Class.forName("languages." + className.substring(0,1).toUpperCase()+className.substring(1).toLowerCase());
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
    
    /**
     * Used to add all Phrases belonging to a language, i.e. adding all Phrase
     * enum values belonging to a Phrase enum class.
     * @param language The language to add Phrases from in String format
     */
    public void addLanguagePhrases(String language) {
        if (language.equalsIgnoreCase("french")) {
            this.phraseBank.addAll(Arrays.asList(French.values()));
        }
        if (language.equalsIgnoreCase("spanish")) {
            this.phraseBank.addAll(Arrays.asList(Spanish.values()));
        }
        if (language.equalsIgnoreCase("german")) {
            this.phraseBank.addAll(Arrays.asList(German.values()));
        }
    }
    
    /**
     * Creates the randomised questions and multichoice answers for running a Game.
     * All test questions are generated from the list of Phrases in the phrase bank
     * at random and all multiple-choice answers are chosen from all Phrases from
     * all languages at random.
     */
    public void generateQuestionSet() {
        generateTestQuestions();
        // For each test question, for each multiChoice set, randomly select the
        // 'incorrect' answers from all phrases (not limited to just the phrase bank)
        Iterator<MultiChoiceQuestion> mcqIter = this.multiChoiceQuestions.iterator();
        while(mcqIter.hasNext()) {
            Collections.shuffle(allPhrases);
            MultiChoiceQuestion multiChoiceQuestion = mcqIter.next();
            
            Question testQuestion = multiChoiceQuestion.getTestQuestion();
            Iterator<Phrase> allPhrasesIter = allPhrases.iterator();
            
            // Add the 'incorrect' multichoice question-answers
            while (multiChoiceQuestion.getQuestions().size() < nMultiChoiceQs) {
                Phrase nextPhrase = allPhrasesIter.next();
                // Only add 'incorrect' choices if they are not identical to the 'correct' test question
                if (!nextPhrase.equals(testQuestion.getPhrase())) {
                    multiChoiceQuestion.addQuestion(new Question(nextPhrase,false));
                }
            }
            // Shuffle the populated multichoice questions so that they are presented at random
            Collections.shuffle(multiChoiceQuestion.getQuestions());
            // Set the correct multichoice answer char/letter
            multiChoiceQuestion.setCorrectAnswerChar();
        }
    }
    
    /**
    * Creates MultiChoiceQuestion objects for the QuestionSet and populates each
    * MultiChoiceQuestion object with the test questions only (no 'incorrect' answers
    * added). 
    */
    private void generateTestQuestions() {
        // Shuffle phraseBank to randomise question order
        Collections.shuffle(this.phraseBank);
        
        // A game must have 20 questions. If the phraseBank has fewer than 20 phrases, 
        // these phrases will be repeated until the quiz contains 20 questions. 
        // If the phraseBank has greater than 20 Phrases then only 20 Phrases will be
        // randomly selected from the phraseBank
        int questionCounter = 0;
        while (questionCounter < nQuestions ) {
            for (Phrase phrase: this.phraseBank) {
                MultiChoiceQuestion multiChoiceQuestion = this.generateNewMultiChoiceQ(phrase);
                this.multiChoiceQuestions.add(multiChoiceQuestion);
                questionCounter++;
                if (questionCounter == nQuestions) {
                    break;
                }   
            }
        }
    }
    
    /**
     * Generates new MultiChoiceQuestion object and populates it with a newly
     * created Question, which is derived. An input parameter defines the Phrase 
     * that is set for this Question and this Question is set to be the test question
     * attribute for the MultiChoiceQuestion object.
     * @param testPhrase Phrase value representing the Phrase used for the
     * test Question for a MultiChoiceQuestion object
     * @return Returns a MultiChoiceQuestion object with the test question defined.
     */
    private MultiChoiceQuestion generateNewMultiChoiceQ(Phrase testPhrase) {
        MultiChoiceQuestion multiChoiceQuestion = new MultiChoiceQuestion();
        // Sets boolean variable true to represent that this it the correct question-answer pair
        multiChoiceQuestion.addQuestion(new Question(testPhrase, true));
        return multiChoiceQuestion;
    }
    
    /**
     * Returns a list of Strings which represent the printed form of all MultiChoiceQuestions
     * in a QuestionSet. This is used for display in the CUI when running a Game. Each
     * String element in the list displays the question and multi-choice answers for
     * one MultiChoiceQuestion object.
     * @return A List of String values represent the printed form of all MultiChoiceQuestions
     * in a QuestionSet. This is used for display in the CUI when running a Game. Each
     * String element in the list displays the question and multi-choice answers for
     * one MultiChoiceQuestion object.
     */
    public List<String> generateQuestionSetDisplay() {
        List<String> questionStringList = new ArrayList<>();
        
        this.multiChoiceQuestions.forEach((m) -> {
            questionStringList.add(m.printQuestion()+"\n"+m.printAnswers());
        });
        return questionStringList;
    }
    
    /**
     * Returns the list of all MultiChoiceQuestion objects belonging to a 
     * QuestionSet
     * @return Returns the list of all MultiChoiceQuestion objects belonging to a 
     * QuestionSet
     */
    public List<MultiChoiceQuestion> getMultiChoiceQuestions() {
        return multiChoiceQuestions;
    }
    
}