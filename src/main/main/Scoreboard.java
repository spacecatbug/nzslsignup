package main;

import java.util.HashMap;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;   

/**
 * Manages the Scoreboard object which is used to save quiz results and to 
 * display the score history of games played request by user input.
 * 
 * @author MYWT
 */
public class Scoreboard {
    
    private static HashMap<String, Integer> scores;

    /**
     * The scores are stored as a HashMap object with the date/time as the keys
     * and the score as the values.
     */
    public Scoreboard() {
        scores = new HashMap<>();
    }
    
    /**
     * Saves the current date and time when a quiz has ended and the score to the
     * scores HashMap.
     * @param score
     */
    public static void saveScore(int score) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();  
        scores.put(dateTimeFormatter.format(now),score);
    }   
    
    /**
     * Returns a String representation of the Scoreboard. It is used to display the 
     * score history when prompted by user input in the CUI.
     * @return Returns a String representation of the Scoreboard.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("High scores table\n");
        for (String scoreInfo: scores.keySet()) {
            String scoreEntry = "Game completed: " + scoreInfo + "\nScore: " + scores.get(scoreInfo)+"\n\n";
            sb.append(scoreEntry);
        }
        // If no scores available then let the user know that they need to play games in order to create a score history
        if (scores.keySet().isEmpty()) {
            sb.append("No scores available. Please complete a game to display scores.\n");
        }
        return sb.toString();
    }
}