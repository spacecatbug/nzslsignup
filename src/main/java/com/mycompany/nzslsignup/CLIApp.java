package com.mycompany.nzslsignup;

import java.io.IOException;
import view.MainMenu;
import view.NZSLChart;

/**
 * This is the main method that runs the application via Command Line Interface
 * @author Megan
 */
public class CLIApp {
    public static void main(String[] args) {
        MainMenu m = new MainMenu();
        try {
            NZSLChart n = new NZSLChart();
        } catch(IOException e) {
            System.err.println(e);
        }
    }
    
}
