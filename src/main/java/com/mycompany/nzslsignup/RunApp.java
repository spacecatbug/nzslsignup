package com.mycompany.nzslsignup;

import controller.Controller;
import model.Model;
import view.View;

/**
 * This is the main method used to run this app.
 * @author Megan
 */
public class RunApp {
    public static void main(String[] args) {
        RunApp runApp = new RunApp();
    }

    public RunApp() {
        Model myModel = new Model();
        View myView = new View();
        myModel.addObserver(myView);
        
        Controller myController = new Controller();
        //pass the reference of model and view to the controllor
        myController.addModel(myModel);
        myController.addView(myView);
        myController.initModel("Main Menu");
        myView.addController(myController);
    }
}
