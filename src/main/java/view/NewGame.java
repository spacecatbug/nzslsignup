package view;

import java.awt.Dimension;
import javax.swing.*;

/**
 * This is the view to setup a new game for this GUI application. It is built as a
 * subclass of JPanel and contributes to the View class.
 * @author Megan
 */
public class NewGame extends JPanel {
    // UI components
    private final JButton standardGameButton;
    private final JButton customGameButton;
    private final JButton mainMenuButton;
    
    // Initialise UI components
    public NewGame() {
        this.standardGameButton = new JButton("Standard Game");
        this.customGameButton = new JButton("Custom Game (load from file)");
        this.mainMenuButton = new JButton("Main Menu");
        setup();
    }
    
    // Setup JPanel
    public void setup() {
        setLayout(null);
        setPreferredSize(new Dimension(630, 450));

        standardGameButton.setLocation(170, 70);
        standardGameButton.setSize(170, 50);
        add(standardGameButton);

        customGameButton.setLocation(170, 130);
        customGameButton.setSize(170, 50);
        add(customGameButton);
        
        mainMenuButton.setLocation(170, 190);
        mainMenuButton.setSize(170, 50);
        add(mainMenuButton);
        
    }
    
    public JButton getStandardGameButton() {
        return this.standardGameButton;
    }
    
    public JButton getCustomGameButton() {
        return this.customGameButton;
    }
    
    public JButton getMainMenuButton() {
        return this.mainMenuButton;
    }
}
