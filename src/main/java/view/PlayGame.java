package view;

import java.awt.*;
import javax.swing.*;

/**
 * This class manages the GUI view for running a game
 * @author Megan
 */
public class PlayGame extends JPanel {
    // UI components
    private final JButton mainMenuButton;
    
    // Initialise UI components
    public PlayGame() {
        this.mainMenuButton = new JButton("Main Menu");
        setup();
    }
    
    // Setup JPanel
    public void setup() {
        setLayout(null);
        setPreferredSize(new Dimension(630, 450));

        mainMenuButton.setLocation(170, 190);
        mainMenuButton.setSize(170, 50);
        add(mainMenuButton);
    }
    
    public JButton getMainMenuButton() {
        return this.mainMenuButton;
    }
    
    
}
