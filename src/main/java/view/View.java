package view;

import controller.Controller;
import java.awt.CardLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

/**
 * This view class manages all views at a high level using the 
 * Swing CardLayout using the MVC design pattern.
 * @author Megan
 */
public class View implements Observer {
    
    private final MainMenu mainMenu;
    private final NewGame newGame;
    private final ScoreBoard scoreBoard;
    private final PlayGame runGame;
    private final CardLayout cardLayout;
    private final JPanel mainPanel;
    private final JFrame frame;
    
    //Components initialization
    public View() {
        frame = new JFrame("NZSL Spell Master");
        this.mainPanel = new JPanel();
        cardLayout = new CardLayout();
        
        mainPanel.setLayout(cardLayout);
        mainMenu = new MainMenu();
        newGame = new NewGame();
        scoreBoard = new ScoreBoard();
        runGame = new PlayGame();

        mainPanel.add(mainMenu, "Main Menu");
        mainPanel.add(newGame, "New Game");
        mainPanel.add(scoreBoard, "Score board");
        mainPanel.add(runGame, "Run Game");
        cardLayout.show(mainPanel, "Main Menu");
        
        frame.add(mainPanel);
        
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300);
        frame.setLocation(100, 100);
        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        cardLayout.show(mainPanel, arg.toString());
    }

    public void addController(Controller controller) {
        //need a controller before adding it as a listener
        mainMenu.getNewGameButton().addActionListener(controller);
        mainMenu.getScoreBoardButton().addActionListener(controller);
        mainMenu.getQuitButton().addActionListener(controller);
        scoreBoard.getMainMenuButton().addActionListener(controller);
        newGame.getMainMenuButton().addActionListener(controller);
        newGame.getStandardGameButton().addActionListener(controller);
        runGame.getMainMenuButton().addActionListener(controller);
    }
    
    public CardLayout getCardLayout() {
        return this.cardLayout;
    }
    
    public JFrame getFrame() {
        return this.frame;
    }
}
