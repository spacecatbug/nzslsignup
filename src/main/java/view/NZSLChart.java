package view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 *
 * @author Megan
 */
public class NZSLChart {
    private JDialog chartDialog;
    private final BufferedImage image;
    public NZSLChart() throws IOException  {
        chartDialog = new JDialog();
        image = ImageIO.read(new File ("src\\main\\resources\\FingerspellingPoster4.png"));
        //chartDialog.setUndecorated(true);
        JLabel label = new JLabel(new ImageIcon(image));
        label.setSize(200, 200);
        JOptionPane.showMessageDialog(null, label, "NZSL Fingerspelling Guide", JOptionPane.PLAIN_MESSAGE, null);
        chartDialog.setSize(200, 200);
        
    }
}
