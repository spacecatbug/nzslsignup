package view;

import javax.swing.*;
import java.awt.*;

/**
 * This is the view for the main menu in the GUI application. It is built as a
 * subclass of JPanel and contributes to the View class.
 * @author Megan
 */
public class MainMenu extends JPanel {
    // UI components
    private final JButton newGameButton;
    private final JButton scoreBoardButton;
    private final JButton quitButton;
    
    // Initialise UI components
    public MainMenu() {
        this.newGameButton = new JButton("New Game");
        this.scoreBoardButton = new JButton("Score board");
        this.quitButton = new JButton("Quit");
        setup();
    }
    
    // Setup JPanel
    public void setup() {
        setLayout(null);
        setPreferredSize(new Dimension(630, 450));

        newGameButton.setLocation(170, 70);
        newGameButton.setSize(170, 50);
        add(newGameButton);

        scoreBoardButton.setLocation(170, 130);
        scoreBoardButton.setSize(170, 50);
        add(scoreBoardButton);
        
        quitButton.setLocation(170, 190);
        quitButton.setSize(170, 50);
        add(quitButton);
    }
    
    public JButton getNewGameButton() {
        return this.newGameButton;
    }
    
    public JButton getScoreBoardButton() {
        return this.scoreBoardButton;
    }
    
    public JButton getQuitButton() {
        return this.quitButton;
    }
}
