package view;

import java.awt.*;
import javax.swing.*;

/**
 * This view displays the past game scores. This class inherits the JPanel class
 * and is part of the CardLayout which displays the main GUI view managed in the
 * CardLayoutView class.
 * @author Megan
 */
public class ScoreBoard extends JPanel {
    // UI components
    private final JButton mainMenuButton;
    
    // Initialise UI components
    public ScoreBoard() {
        this.mainMenuButton = new JButton("Main Menu");
        setup();
    }
    
    // Setup JPanel
    public void setup() {
        setLayout(null);
        setPreferredSize(new Dimension(630, 450));

        JLabel titleLabel = new JLabel("High scores");
        titleLabel.setLocation(50,50);
        add(titleLabel);
        
        mainMenuButton.setLocation(170, 70);
        mainMenuButton.setSize(170, 50);
        add(mainMenuButton);
    }
    
    public JButton getMainMenuButton() {
        return this.mainMenuButton;
    }

}
