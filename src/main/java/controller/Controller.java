package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.GameModel;
import model.Model;
import view.View;

/**
 * This is the Controller class for this application following the MVC
 * design structure.
 * @author Megan
 */
public class Controller implements ActionListener{
    private Model model;
    private View view;
    
    public Controller() {
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String buttonPressed = e.getActionCommand();
        if (buttonPressed.equals("Quit")) {
            view.getFrame().dispose();
        } else if (buttonPressed.equals("Standard Game")) {
            System.out.println("Start new game");
            model.setMenu("Run Game");
        } else {
            model.setMenu(buttonPressed);
        }
    }
    
    public void addModel(Model m) {
        this.model = m;
    }

    public void addView(View v) {
        this.view = v;
    }

    public void initModel(String menuState) {
        model.setMenu(menuState);
    }
}
