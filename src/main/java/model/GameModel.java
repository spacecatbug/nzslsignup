package model;

import java.util.Observable;

/**
 * This model maintains the Game objects which are used when a game is running.
 * @author Megan
 */
public class GameModel extends Observable {
    private Game game;
    
    /* When model is first initialised and the app first starts, the main
    menu is shown */
    public GameModel() {
        this.game = new Game();
    }
    
    public void setGameScore(Integer score) {
        this.game.setScore(score);
        setChanged();
        notifyObservers(this.game);
    }
    
    public void setInProgress(boolean inProgress) {
        this.game.setInProgress(inProgress);
        setChanged();
        notifyObservers(this.game);
    }
}