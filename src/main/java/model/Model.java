package model;

import java.util.Observable;

/**
 * This class maintains the model component of the MVC design pattern.
 * The model class contains the app state which is used to control the 
 * navigation to indicate which view should be displayed when the app is in use. 
 * The model also contains a Game object which maintains data when a game is 
 * being played.
 * @author Megan
 */
public class Model extends Observable {
    private String appState;
    private Game game;
    
    /* When model is first initialised and the app first starts, the main
    menu is shown */
    public Model() {
        this.appState = "Main Menu";
        this.game = new Game();
    }
    
    public void setMenu(String appState) {
        this.appState = appState;
        setChanged();
        notifyObservers(appState);
    }
    
    public void updateGame(Game game) {
        this.game = game;
        notifyObservers(game);
    }
}
