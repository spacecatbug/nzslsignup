package model;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Randomly generates characters for the player to guess when running a game.
 * @author Megan
 */
public class LetterGenerator {
    private final ThreadLocalRandom rand;
    private ArrayList<String> letterBank;
    
    // No letter bank specified in constructor - default to using all alphabet letters for the game
    public LetterGenerator() {
        rand = ThreadLocalRandom.current();
    }
    
    public char nextLetter() {
        return (char) rand.nextInt(65,91);
    }
}
