package model;

/**
 * This class manages the variables for the current game session
 * @author Megan
 */
public class Game {
    private boolean inProgress;
    private int score;
    
    public Game() {
        inProgress = true;
        score = 0;
    }
    
    public int getScore() {
        return score;
    }
    
    public void setScore(int score) {
        this.score = score;
    }
    
    public boolean getInProgress() {
        return inProgress;
    }
    
    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }
    
    public char getLetter() {
        LetterGenerator letterGenerator = new LetterGenerator();
        return letterGenerator.nextLetter();
    }
}
