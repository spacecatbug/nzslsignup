package model;

import java.util.Scanner;

/**
 * Loads a text file of characters used to create a customised game.
 * @author Megan
 */
public class LoadCustomGameFile {
    private String filePath;
    private String fileString;
    private Scanner fileReader;
    
    public LoadCustomGameFile(String filePath) {
        this.filePath = filePath;
        
    }
}
